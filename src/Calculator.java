import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Double result;
        String event;
        Scanner reader = new Scanner(System.in);
        System.out.print("Введите первое число:\n");
        Double number1 = reader.nextDouble();
        System.out.print("Введите второе число:\n");
        Double number2 = reader.nextDouble();
        System.out.print("Введите оператор вычисления: (+, -, *, /, sin, cos, min, max): ");
        event = reader.next();

        switch (event) {
            case "+":
                result = number1 + number2; // сложени
                System.out.println("Итого у нас получилось: " + number1 + " " + event + " " + number2 + " = " + result);
                break;
            case "-":
                result = number1 - number2; // вычитание
                System.out.println("Итого у нас получилось: " + number1 + " " + event + " " + number2 + " = " + result);
                break;
            case "*":
                result = number1 * number2; // умножени
                System.out.println("Итого у нас получилось: " + number1 + " " + event + " " + number2 + " = " + result);
                break;
            case "/":
                result = number1 / number2; // деление
                System.out.println("Итого у нас получилось: " + number1 + " " + event + " " + number2 + " = " + result);
                break;
            case "sin":
                result = Math.sin(number1);
                System.out.println("Итого у нас получилось: " + event + " (" + number1 + ") = " + result);
                break;
            case "cos":
                result = Math.cos(number1);
                System.out.println("Итого у нас получилось: " + event + " (" + number1 + ") = " + result);
                break;
            case "tg":
                result = Math.tan(number1);
                System.out.println("Итого у нас получилось: " + event + " (" + number1 + ") = " + result);
                break;
            case "min":
                result = Math.min(number1, number2); //выбор минимального числа
                System.out.println("Итого у нас получилось: " + event + "(" + number1 + "," + number2 + ") = " + result);
                break;
            case "max":
                result = Math.max(number1, number2); //выбор максимального числа
                System.out.println("Итого у нас получилось: " + event + "(" + number1 + "," + number2 + ") = " + result);
                break;
            default:
                System.out.printf("Ошибочка введите оператор вычисления из предложенных");
                reader.close();
                return;
        }
        reader.close();
    }
}
